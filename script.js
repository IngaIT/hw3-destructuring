import {clients1, clients2, characters, user1, satoshi2018, satoshi2019, satoshi2020, books, bookToAdd, employee, array} from "./database.js";

//1.
const clients = [...new Set([...clients1, ...clients2])];
console.log(clients);

//2.
const charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}));
console.log(charactersShortInfo);

//3.
const {name: firstName, years: age, isAdmin = false} = user1;
console.log(firstName, age, isAdmin);

//4.
const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);

//5.
const newBooks = [...books, bookToAdd];
console.log(newBooks);

//6.
const newEmployee = {...employee, age: 45, salary: 50000};
console.log(newEmployee);

//7.
const [value, showValue] = array;
alert(value);
alert(showValue());




